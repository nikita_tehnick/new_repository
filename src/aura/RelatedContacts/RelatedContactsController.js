({
    doInit:function (component, event, helper) {
        helper.init(component);
    },
    changeMode:function (component, event, helper) {
        component.set('v.flag', true);
    },
    clickSave:function(component, event, helper){
        helper.saving(component);
    },
    clickCancel:function(component, event, helper){
        component.set('v.flag', false);
    },
});