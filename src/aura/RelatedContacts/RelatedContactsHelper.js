({
    init:function (component) {
        var accId  = component.get('v.recordId');
        var action = component.get('c.getRelatedContacts');
        action.setParams({
            'accId' : accId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                component.set("v.contacts", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    saving:function(component){
        var contacts = component.get('v.contacts');
        var action = component.get('c.upsertContacts');
        action.setParams({
            'contacts' : contacts
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set('v.flag', false);
            }
        });
        $A.enqueueAction(action);
    },
});