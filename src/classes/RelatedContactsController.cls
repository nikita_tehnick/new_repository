public with sharing class RelatedContactsController {
    @AuraEnabled
    public static List<Contact> getRelatedContacts(string accId){
        List<Contact> relatedContacts = [SELECT Id,FirstName, LastName, Title, Email, AccountId FROM Contact WHERE AccountId = :accId];
        return relatedContacts;
    }

    @AuraEnabled
    public static void upsertContacts(List<Contact> contacts){
        upsert contacts;
    }
}